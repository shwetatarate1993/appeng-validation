export declare enum Mode {
    INSERT = "INSERT",
    UPDATE = "UPDATE",
    BOTH = "BOTH"
}
export declare const DB: string;
export declare enum DataType {
    NUMBER = "NUMBER",
    EMAIL = "EMAIL",
    ALPHABET = "ALPHABET",
    DECIMAL = "DECIMAL"
}
export declare const EXECUTION_MODE: string;
export declare enum Separator {
    Navigate = "=>"
}
export declare const RECORD_INDEX: string;
export declare const ENGLISH_LOCALE: string;
//# sourceMappingURL=appeng.constant.d.ts.map