"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Mode;
(function (Mode) {
    Mode["INSERT"] = "INSERT";
    Mode["UPDATE"] = "UPDATE";
    Mode["BOTH"] = "BOTH";
})(Mode = exports.Mode || (exports.Mode = {}));
exports.DB = 'db';
var DataType;
(function (DataType) {
    DataType["NUMBER"] = "NUMBER";
    DataType["EMAIL"] = "EMAIL";
    DataType["ALPHABET"] = "ALPHABET";
    DataType["DECIMAL"] = "DECIMAL";
})(DataType = exports.DataType || (exports.DataType = {}));
exports.EXECUTION_MODE = 'b786d7ed-9644-11e9-ad17-12d8bca32300';
var Separator;
(function (Separator) {
    Separator["Navigate"] = "=>";
})(Separator = exports.Separator || (exports.Separator = {}));
exports.RECORD_INDEX = '946ea30c-caf6-11e9-8548-0ee72c6ddce6';
exports.ENGLISH_LOCALE = "en";
//# sourceMappingURL=appeng.constant.js.map