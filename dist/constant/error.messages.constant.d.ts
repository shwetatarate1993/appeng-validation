export declare const MANDATORY_MSG: string;
export declare const INVALID_LENGTH_MSG: string;
export declare const INVALID_NUMBER: string;
export declare const INVALID_EMAIL_ADDRESS: string;
export declare const INVALID_DECIMAL_MSG: string;
export declare const ALPHABETS_ONLY_MSG: string;
export declare const VALIDATION_SUCCESS: string;
export declare const VALIDATION_FAILED: string;
//# sourceMappingURL=error.messages.constant.d.ts.map