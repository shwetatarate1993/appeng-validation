"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MANDATORY_MSG = 'en-!lAnGuAge!-Is Required-!sEpArAToR!-gu-!lAnGuAge!-જરૂરી છે';
exports.INVALID_LENGTH_MSG = 'en-!lAnGuAge!-Invalid Length.Max length is upto ' +
    '-!sEpArAToR!-gu-!lAnGuAge!-અમાન્ય લંબાઈ.મેક્સ લંબાઈ સુધી છે ';
exports.INVALID_NUMBER = 'en-!lAnGuAge!-Invalid Number-!sEpArAToR!-gu-!lAnGuAge!-અમાન્ય નંબર';
exports.INVALID_EMAIL_ADDRESS = 'en-!lAnGuAge!-Invalid Email Address-!sEpArAToR!-gu-!lAnGuAge!-અમાન્ય ઇમેઇલ સરનામું';
exports.INVALID_DECIMAL_MSG = 'en-!lAnGuAge!-Invalid Value.Number allowed up to two decimal point' +
    '-!sEpArAToR!-gu-!lAnGuAge!-અમાન્ય મૂલ્ય.નંબરે બે દશાંશ બિંદુ સુધી મંજૂરી આપી';
exports.ALPHABETS_ONLY_MSG = 'en-!lAnGuAge!-Allowed alphabets only' +
    '-!sEpArAToR!-gu-!lAnGuAge!-માત્ર માન્ય મૂળાક્ષરો';
exports.VALIDATION_SUCCESS = 'Validated Succesfully';
exports.VALIDATION_FAILED = 'Validation Failed';
//# sourceMappingURL=error.messages.constant.js.map