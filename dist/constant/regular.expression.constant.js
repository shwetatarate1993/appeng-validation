"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NEMBER_REGEX = /^\d+$/;
exports.EMAIL_REGEX = /\S+@\S+\.\S+/;
exports.DECIMAL_REGEX = /^\d+(\.\d{1,2})?$/;
exports.ALPHABETS_ONLY_REGEX = /[A-Z]/ig;
//# sourceMappingURL=regular.expression.constant.js.map