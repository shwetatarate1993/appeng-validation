import { EntityColumn } from '../models';
import { IDataValidationExecutor } from './';
export declare class DataIsRequiredValidator implements IDataValidationExecutor {
    executeValidation(data: any, column: EntityColumn): Promise<string>;
}
declare const dataIsRequiredValidator: DataIsRequiredValidator;
export default dataIsRequiredValidator;
//# sourceMappingURL=data.isrequired.validator.d.ts.map