"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constant_1 = require("../constant");
const info_commons_1 = require("info-commons");
class DataIsRequiredValidator {
    async executeValidation(data, column) {
        let errorMsg = '';
        const value = data[column.dbCode];
        const currentLang = data.APP_CURRENT_LANGUAGE ? data.APP_CURRENT_LANGUAGE : constant_1.ENGLISH_LOCALE;
        if (column.isMandatory && (value === undefined || value === null || value === "")) {
            errorMsg = info_commons_1.extractLabel(constant_1.MANDATORY_MSG, currentLang);
        }
        return errorMsg;
    }
}
exports.DataIsRequiredValidator = DataIsRequiredValidator;
const dataIsRequiredValidator = new DataIsRequiredValidator();
Object.freeze(dataIsRequiredValidator);
exports.default = dataIsRequiredValidator;
//# sourceMappingURL=data.isrequired.validator.js.map