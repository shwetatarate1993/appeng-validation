import { EntityColumn } from '../models';
import { IDataValidationExecutor, IValidatorOptionalParams } from './';
export declare class DataIsUniqueValidator implements IDataValidationExecutor {
    executeValidation(data: any, column: EntityColumn, params: IValidatorOptionalParams): Promise<string>;
}
declare const dataIsUniqueValidator: DataIsUniqueValidator;
export default dataIsUniqueValidator;
//# sourceMappingURL=data.isunique.validator.d.ts.map