import { EntityColumn } from '../models';
import { IDataValidationExecutor } from './';
export declare class DataLengthValidator implements IDataValidationExecutor {
    executeValidation(data: any, column: EntityColumn): Promise<string>;
}
declare const dataLengthValidator: DataLengthValidator;
export default dataLengthValidator;
//# sourceMappingURL=data.length.validator.d.ts.map