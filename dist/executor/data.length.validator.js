"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constant_1 = require("../constant");
const info_commons_1 = require("info-commons");
class DataLengthValidator {
    async executeValidation(data, column) {
        let errorMsg = '';
        const value = data[column.dbCode];
        if (value && column.length && column.length > 0) {
            const currentLang = data.APP_CURRENT_LANGUAGE ? data.APP_CURRENT_LANGUAGE : constant_1.ENGLISH_LOCALE;
            if (value.length > column.length) {
                errorMsg = info_commons_1.extractLabel(constant_1.INVALID_LENGTH_MSG, currentLang) + column.length;
            }
        }
        return errorMsg;
    }
}
exports.DataLengthValidator = DataLengthValidator;
const dataLengthValidator = new DataLengthValidator();
Object.freeze(dataLengthValidator);
exports.default = dataLengthValidator;
//# sourceMappingURL=data.length.validator.js.map