import { EntityColumn } from '../models';
import { IDataValidationExecutor } from './';
export declare class DataTypeValidator implements IDataValidationExecutor {
    executeValidation(data: any, column: EntityColumn): Promise<string>;
}
declare const dataTypeValidator: DataTypeValidator;
export default dataTypeValidator;
//# sourceMappingURL=data.type.validator.d.ts.map