"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constant_1 = require("../constant");
const info_commons_1 = require("info-commons");
class DataTypeValidator {
    async executeValidation(data, column) {
        let errorMsg = '';
        const value = data[column.dbCode];
        const currentLang = data.APP_CURRENT_LANGUAGE ? data.APP_CURRENT_LANGUAGE : constant_1.ENGLISH_LOCALE;
        if (value) {
            switch (column.dataType) {
                case constant_1.DataType.NUMBER:
                    if (!constant_1.NEMBER_REGEX.test(value)) {
                        errorMsg = info_commons_1.extractLabel(constant_1.INVALID_NUMBER, currentLang);
                    }
                    break;
                case constant_1.DataType.EMAIL:
                    if (!constant_1.EMAIL_REGEX.test(value)) {
                        errorMsg = info_commons_1.extractLabel(constant_1.INVALID_EMAIL_ADDRESS, currentLang);
                    }
                    break;
                case constant_1.DataType.DECIMAL:
                    if (!constant_1.DECIMAL_REGEX.test(value)) {
                        errorMsg = info_commons_1.extractLabel(constant_1.INVALID_DECIMAL_MSG, currentLang);
                    }
                    break;
                case constant_1.DataType.ALPHABET:
                    if (!constant_1.ALPHABETS_ONLY_REGEX.test(value)) {
                        errorMsg = info_commons_1.extractLabel(constant_1.ALPHABETS_ONLY_MSG, currentLang);
                    }
                    break;
            }
        }
        return errorMsg;
    }
}
exports.DataTypeValidator = DataTypeValidator;
const dataTypeValidator = new DataTypeValidator();
Object.freeze(dataTypeValidator);
exports.default = dataTypeValidator;
//# sourceMappingURL=data.type.validator.js.map