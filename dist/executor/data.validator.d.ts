import { EntityColumn } from '../models';
import { IDataValidationExecutor, ILogicalColumnValidator, IValidatorOptionalParams } from './';
export declare class DataValidator implements ILogicalColumnValidator {
    readonly dataExecutors: IDataValidationExecutor[];
    constructor();
    validate(data: any, column: EntityColumn, params: IValidatorOptionalParams): Promise<string[]>;
}
declare const dataValidator: DataValidator;
export default dataValidator;
//# sourceMappingURL=data.validator.d.ts.map