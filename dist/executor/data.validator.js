"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const data_isrequired_validator_1 = __importDefault(require("./data.isrequired.validator"));
const data_isunique_validator_1 = __importDefault(require("./data.isunique.validator"));
const data_length_validator_1 = __importDefault(require("./data.length.validator"));
const data_type_validator_1 = __importDefault(require("./data.type.validator"));
class DataValidator {
    constructor() {
        const dataExecutors = [];
        dataExecutors.push(data_length_validator_1.default);
        dataExecutors.push(data_type_validator_1.default);
        dataExecutors.push(data_isrequired_validator_1.default);
        dataExecutors.push(data_isunique_validator_1.default);
        this.dataExecutors = dataExecutors;
    }
    async validate(data, column, params) {
        const errorList = [];
        for (const executor of this.dataExecutors) {
            const errorMsg = await executor.executeValidation(data, column, params);
            if (errorMsg) {
                errorList.push(errorMsg);
            }
        }
        return errorList;
    }
}
exports.DataValidator = DataValidator;
const dataValidator = new DataValidator();
Object.freeze(dataValidator);
exports.default = dataValidator;
//# sourceMappingURL=data.validator.js.map