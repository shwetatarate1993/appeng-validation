import { EntityColumn } from '../models';
import { IValidatorOptionalParams } from './';
export interface IDataValidationExecutor {
    executeValidation(data: any, column: EntityColumn, optionalParams: IValidatorOptionalParams): Promise<string>;
}
//# sourceMappingURL=idata.validation.executor.d.ts.map