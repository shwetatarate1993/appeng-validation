import { EntityColumn } from '../models';
import { IValidatorOptionalParams } from './';
export interface ILogicalColumnValidator {
    validate(data: any, column: EntityColumn, optinalParams: IValidatorOptionalParams): Promise<string[]>;
}
//# sourceMappingURL=ilogical.column.validator.d.ts.map