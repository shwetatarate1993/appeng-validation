import { ServiceOrchestrator } from 'appeng-db';
import { Mode } from '../constant';
export interface IValidatorOptionalParams {
    mode?: Mode;
    serviceOrchestrator?: ServiceOrchestrator;
}
//# sourceMappingURL=ivalidator.optional.params.d.ts.map