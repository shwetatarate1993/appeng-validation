import { EntityColumn } from '../models';
import { ILogicalColumnValidator, IValidatorOptionalParams } from './';
export declare class LogicalColumnDBValidator implements ILogicalColumnValidator {
    validate(data: any, column: EntityColumn, params: IValidatorOptionalParams): Promise<string[]>;
}
declare const logicalColumnDBValidator: LogicalColumnDBValidator;
export default logicalColumnDBValidator;
//# sourceMappingURL=logical.column.db.validator.d.ts.map