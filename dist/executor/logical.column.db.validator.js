"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const appengutil_1 = require("../utils/appengutil");
class LogicalColumnDBValidator {
    async validate(data, column, params) {
        const errorList = [];
        const value = data[column.dbCode];
        if (value && column.databaseValidations &&
            column.databaseValidations.length > 0) {
            for (const validation of column.databaseValidations) {
                if (appengutil_1.isExecutableWithMode(validation.mode, params.mode)) {
                    if (!validation.isConditionAvailable || (validation.conditionExpression
                        && appengutil_1.isExpressionResolved(validation.conditionExpression, data))) {
                        const result = await params.serviceOrchestrator.selectRecordForValidationObject(validation, data);
                        if (!appengutil_1.isExpressionResolved(validation.validationExpression, result)) {
                            errorList.push(validation.validationMessage);
                        }
                    }
                }
            }
        }
        return errorList;
    }
}
exports.LogicalColumnDBValidator = LogicalColumnDBValidator;
const logicalColumnDBValidator = new LogicalColumnDBValidator();
Object.freeze(logicalColumnDBValidator);
exports.default = logicalColumnDBValidator;
//# sourceMappingURL=logical.column.db.validator.js.map