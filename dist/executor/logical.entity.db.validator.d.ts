import { ServiceOrchestrator } from 'appeng-db';
import { Mode } from '../constant';
import { LogicalEntity } from '../models';
export declare class LogicalEntityDBValidator {
    validate(data: any, entity: LogicalEntity, mode: Mode, serviceOrchestrator: ServiceOrchestrator): Promise<string[]>;
}
declare const logicalEntityDBValidator: LogicalEntityDBValidator;
export default logicalEntityDBValidator;
//# sourceMappingURL=logical.entity.db.validator.d.ts.map