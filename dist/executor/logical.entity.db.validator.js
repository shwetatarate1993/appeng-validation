"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const appengutil_1 = require("../utils/appengutil");
class LogicalEntityDBValidator {
    async validate(data, entity, mode, serviceOrchestrator) {
        const errorList = [];
        if (entity.formDbValidations != null &&
            entity.formDbValidations.length > 0) {
            for (const validation of entity.formDbValidations) {
                if (appengutil_1.isExecutableWithMode(validation.mode, mode)) {
                    if (!validation.isConditionAvailable || (validation.conditionExpression
                        && appengutil_1.isExpressionResolved(validation.conditionExpression, data))) {
                        const result = await serviceOrchestrator.selectRecordForValidationObject(validation, data);
                        if (!appengutil_1.isExpressionResolved(validation.validationExpression, result)) {
                            errorList.push(validation.validationMessage);
                        }
                    }
                }
            }
        }
        return errorList;
    }
}
exports.LogicalEntityDBValidator = LogicalEntityDBValidator;
const logicalEntityDBValidator = new LogicalEntityDBValidator();
Object.freeze(logicalEntityDBValidator);
exports.default = logicalEntityDBValidator;
//# sourceMappingURL=logical.entity.db.validator.js.map