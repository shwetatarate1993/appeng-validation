import { EntityColumn } from '../models';
import { ILogicalColumnValidator, IValidatorOptionalParams } from './';
export declare class RegexValidator implements ILogicalColumnValidator {
    validate(data: any, column: EntityColumn, params: IValidatorOptionalParams): Promise<string[]>;
}
declare const regexValidator: RegexValidator;
export default regexValidator;
//# sourceMappingURL=regex.validator.d.ts.map