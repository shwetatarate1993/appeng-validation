"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const appengutil_1 = require("../utils/appengutil");
class RegexValidator {
    async validate(data, column, params) {
        const errorList = [];
        if (column.standardValidations &&
            column.standardValidations.length > 0) {
            const value = data[column.dbCode];
            for (const validation of column.standardValidations) {
                if (appengutil_1.isExecutableWithMode(validation.mode, params.mode)) {
                    if (!validation.isConditionAvailable || (validation.conditionExpression
                        && appengutil_1.isExpressionResolved(validation.conditionExpression, data))) {
                        const regex = new RegExp(validation.regex);
                        if (!regex.test(value)) {
                            errorList.push(validation.defaultErrorMessage);
                        }
                    }
                }
            }
        }
        return errorList;
    }
}
exports.RegexValidator = RegexValidator;
const regexValidator = new RegexValidator();
Object.freeze(regexValidator);
exports.default = regexValidator;
//# sourceMappingURL=regex.validator.js.map