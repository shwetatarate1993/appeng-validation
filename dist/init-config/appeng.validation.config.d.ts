import { DBValidatorForClient, Validator } from '../services';
export default class AppengValidationConfig {
    static readonly INSTANCE: Validator;
    static readonly DB_VALIDATOR_INSTANCE: DBValidatorForClient;
    static configure(config: any, configuredDialect: string): void;
    private static validator;
    private static dbValidatorForClient;
    private constructor();
}
//# sourceMappingURL=appeng.validation.config.d.ts.map