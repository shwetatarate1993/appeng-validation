"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const appeng_db_1 = require("appeng-db");
const services_1 = require("../services");
class AppengValidationConfig {
    constructor() {
        /** No Op */
    }
    static get INSTANCE() {
        return AppengValidationConfig.validator;
    }
    static get DB_VALIDATOR_INSTANCE() {
        return AppengValidationConfig.dbValidatorForClient;
    }
    static configure(config, configuredDialect) {
        if (!AppengValidationConfig.validator
            && !AppengValidationConfig.dbValidatorForClient) {
            appeng_db_1.AppengDBConfig.configure(config, configuredDialect);
        }
        if (!AppengValidationConfig.validator) {
            const validator = services_1.createValidateService(appeng_db_1.AppengDBConfig.INSTANCE);
            AppengValidationConfig.validator = validator;
        }
        if (!AppengValidationConfig.dbValidatorForClient) {
            const dbValidator = services_1.createDBValidatorForClient(appeng_db_1.AppengDBConfig.INSTANCE);
            AppengValidationConfig.dbValidatorForClient = dbValidator;
        }
    }
}
exports.default = AppengValidationConfig;
//# sourceMappingURL=appeng.validation.config.js.map