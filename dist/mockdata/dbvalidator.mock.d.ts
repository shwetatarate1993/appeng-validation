declare const _default: {
    validation: {
        configObjectId: any;
        name: string;
        configObjectType: any;
        createdBy: any;
        isDeleted: number;
        itemDescription: any;
        creationDate: any;
        projectId: number;
        updatedBy: any;
        updationDate: any;
        deletionDate: any;
        datasourceName: string;
        datasourceType: string;
        validationType: string;
        validationExpression: string;
        validationQid: string;
        validationMessage: string;
        validationExpressionKeys: string;
    };
    data: {
        'APP_LOGGED_IN_SUB_PROJECT_ID': string;
        'APP_LOGGED_IN_YEAR': string;
        'APP_LOGGED_IN_USER_DISTRICT': string;
        'APP_CONFIGURATION_PROJECT_ID': string;
        'APP_CURRENT_DATE': string;
        'APP_LOGGED_IN_USER_STATE': string;
        'APP_LOGGED_IN_USER_GP': string;
        'APP_LOGGED_IN_USER_TALUKA': string;
        'APP_LOGGED_IN_YEAR_NAME': string;
        'APP_LOGGED_IN_USER_ID': string;
        'isProcessed': boolean;
        'isRenderingRequired': string;
        'NAME': string;
        'isDirty': string;
        'b786d7ed-9644-11e9-ad17-12d8bca32300': string;
    };
};
export default _default;
//# sourceMappingURL=dbvalidator.mock.d.ts.map