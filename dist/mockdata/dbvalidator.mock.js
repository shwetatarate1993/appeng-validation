"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    validation: {
        configObjectId: null,
        name: 'Unique Name',
        configObjectType: null,
        createdBy: null,
        isDeleted: 0,
        itemDescription: null,
        creationDate: null,
        projectId: 0,
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
        datasourceName: 'METADB',
        datasourceType: 'RDBMS',
        validationType: 'warning',
        validationExpression: '#{EXISTING_NAME_COUNT == 0}',
        validationQid: 'SELECT COUNT(*) AS EXISTING_NAME_COUNT FROM Company where UPPER(NAME) = UPPER(:NAME) and ID!=COALESCE(:ID,0)',
        validationMessage: 'Name Already Exist',
        validationExpressionKeys: 'EXISTING_NAME_COUNT',
    },
    data: {
        'APP_LOGGED_IN_SUB_PROJECT_ID': '1',
        'APP_LOGGED_IN_YEAR': '1',
        'APP_LOGGED_IN_USER_DISTRICT': '940803e9-56a9-11e9-b039-0ee72c6ddce6',
        'APP_CONFIGURATION_PROJECT_ID': '1',
        'APP_CURRENT_DATE': '2019-05-28',
        'APP_LOGGED_IN_USER_STATE': 'e8a18ea2-56a7-11e9-b039-0ee72c6ddce6',
        'APP_LOGGED_IN_USER_GP': 'null',
        'APP_LOGGED_IN_USER_TALUKA': 'null',
        'APP_LOGGED_IN_YEAR_NAME': '2017-2018',
        'APP_LOGGED_IN_USER_ID': '1137',
        'isProcessed': true,
        'isRenderingRequired': 'false',
        'NAME': 'InfoOrigin',
        'isDirty': 'true',
        'b786d7ed-9644-11e9-ad17-12d8bca32300': 'INSERT',
    },
};
//# sourceMappingURL=dbvalidator.mock.js.map