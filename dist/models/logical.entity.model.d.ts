import { DatabaseValidation, EntityColumn } from './';
export default class LogicalEntity {
    readonly configObjectId: string;
    readonly name: string;
    readonly configObjectType: string;
    readonly createdBy: string;
    readonly isDeleted: number;
    readonly itemDescription: string;
    readonly creationDate: Date;
    readonly projectId: number;
    readonly updatedBy: string;
    readonly updationDate: Date;
    readonly deletionDate: Date;
    readonly dbTypeName: string;
    readonly supportedFlavor: string;
    readonly generateSkeleton: boolean;
    readonly logicalColumns: EntityColumn[];
    readonly formDbValidations: DatabaseValidation[];
    getPropertyMap: () => {
        jsonNameWithDBCodeMap: any;
        dBCodeWithDatatypeMap: any;
    };
}
//# sourceMappingURL=logical.entity.model.d.ts.map