import { PhysicalEntity } from './';
export default class PhysicalData {
    readonly dbOperationType: string;
    readonly physicalEntity: PhysicalEntity;
    readonly newData: any;
    constructor(dbOperationType: string, physicalEntity: PhysicalEntity, newData: any);
}
//# sourceMappingURL=physical.data.model.d.ts.map