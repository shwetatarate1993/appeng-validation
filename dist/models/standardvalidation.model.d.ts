import { ConfigItem, ConfigItemPrivilege, ConfigItemRelation } from 'meta-db';
import { Mode } from '../constant';
export default class StandardValidation implements ConfigItem {
    static deserialize(input: any): StandardValidation;
    readonly configObjectId: string;
    readonly name: string;
    readonly configObjectType: string;
    readonly createdBy: string;
    readonly isDeleted: number;
    readonly itemDescription: string;
    readonly creationDate: Date;
    readonly projectId: number;
    readonly updatedBy: string;
    readonly updationDate: Date;
    readonly deletionDate: Date;
    readonly defaultErrorMessage: string;
    readonly validationType: string;
    readonly regex: string;
    readonly isConditionAvailable: boolean;
    readonly privileges?: ConfigItemPrivilege[];
    readonly childRelations?: ConfigItemRelation[];
    readonly parentRelations?: ConfigItemRelation[];
    readonly mode: Mode;
    readonly conditionExpression: string;
    constructor(itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, defaultErrorMessage: string, validationType: string, regex: string, isConditionAvailable: boolean, privileges?: ConfigItemPrivilege[], childRelations?: ConfigItemRelation[], parentRelations?: ConfigItemRelation[]);
}
//# sourceMappingURL=standardvalidation.model.d.ts.map