"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ValidationResponse {
    constructor(fieldId, value, errorMessages) {
        this.fieldId = fieldId;
        this.value = value;
        this.errorMessages = errorMessages;
        Object.freeze(this);
    }
}
exports.default = ValidationResponse;
//# sourceMappingURL=validation.response.model.js.map