export { default as logicalColumnValidationScope } from './logical.column.validation.scope';
export { default as logicalEntityValidationScope } from './logical.entity.validation.scope';
export { IValidationScope } from './ivalidation.scope';
//# sourceMappingURL=index.d.ts.map