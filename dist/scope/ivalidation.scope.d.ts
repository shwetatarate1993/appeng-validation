import { ServiceOrchestrator } from 'appeng-db';
import { Errors } from 'info-commons';
import { EntityGroupData } from '../models';
export interface IValidationScope {
    validateAll(entityGroupData: EntityGroupData, serviceOrchestrator: ServiceOrchestrator): Promise<Errors[]>;
}
//# sourceMappingURL=ivalidation.scope.d.ts.map