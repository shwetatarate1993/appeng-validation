import { ServiceOrchestrator } from 'appeng-db';
import { Errors } from 'info-commons';
import { ILogicalColumnValidator } from '../executor';
import { EntityGroupData, LogicalData } from '../models';
import { IValidationScope } from './';
export declare class LogicalColumnValidationScope implements IValidationScope {
    readonly columnExecutors: ILogicalColumnValidator[];
    constructor();
    validateAll(entityGroupData: EntityGroupData, serviceOrchestrator: ServiceOrchestrator): Promise<Errors[]>;
    validateData(logicalData: LogicalData, serviceOrchestrator: ServiceOrchestrator, errorResponse: Errors[], referenceData: any, parentLocation: string): Promise<void>;
}
declare const logicalColumnValidationScope: LogicalColumnValidationScope;
export default logicalColumnValidationScope;
//# sourceMappingURL=logical.column.validation.scope.d.ts.map