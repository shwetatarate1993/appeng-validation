'use strict';
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const cloneDeep_1 = __importDefault(require("lodash/cloneDeep"));
const constant_1 = require("../constant");
const executor_1 = require("../executor");
const appengutil_1 = require("../utils/appengutil");
class LogicalColumnValidationScope {
    constructor() {
        const columnExecutors = [];
        columnExecutors.push(executor_1.dataValidator);
        columnExecutors.push(executor_1.logicalColumnDBValidator);
        columnExecutors.push(executor_1.regexValidator);
        this.columnExecutors = columnExecutors;
    }
    async validateAll(entityGroupData, serviceOrchestrator) {
        const errorResponse = [];
        await this.validateData(entityGroupData.logicalData, serviceOrchestrator, errorResponse, entityGroupData.referenceData, '');
        return errorResponse;
    }
    async validateData(logicalData, serviceOrchestrator, errorResponse, referenceData, parentLocation) {
        const logicalEntity = logicalData.ceNode.entity;
        let entityLocation = parentLocation +
            logicalEntity.configObjectId + constant_1.Separator.Navigate;
        for (const lcData of logicalData.appData.data) {
            const data = cloneDeep_1.default(lcData);
            Object.assign(data, referenceData);
            const filteredColumnsByMode = logicalEntity.logicalColumns.filter((lc) => appengutil_1.isExecutableWithMode(lc.mode, data[constant_1.EXECUTION_MODE]));
            for (const lc of filteredColumnsByMode) {
                const errorList = [];
                for (const columnExecutor of this.columnExecutors) {
                    const errorMessages = await columnExecutor.validate(data, lc, { mode: data[constant_1.EXECUTION_MODE], serviceOrchestrator });
                    errorList.push.apply(errorList, errorMessages);
                }
                if (errorList.length > 0) {
                    const fieldId = lc.jsonName ?
                        lc.jsonName : lc.dbCode;
                    const recordIndex = data[constant_1.RECORD_INDEX] !== undefined
                        ? data[constant_1.RECORD_INDEX] : logicalData.appData.data.indexOf(lcData);
                    const fieldResponse = new info_commons_1.Errors();
                    fieldResponse.message = errorList.toString();
                    fieldResponse.reason = errorList.toString();
                    fieldResponse.location = entityLocation + recordIndex
                        + constant_1.Separator.Navigate + fieldId;
                    errorResponse.push(fieldResponse);
                }
            }
        }
        for (const ld of logicalData.childLogicalData) {
            entityLocation = entityLocation + '0' + constant_1.Separator.Navigate;
            await this.validateData(ld, serviceOrchestrator, errorResponse, referenceData, entityLocation);
        }
    }
}
exports.LogicalColumnValidationScope = LogicalColumnValidationScope;
const logicalColumnValidationScope = new LogicalColumnValidationScope();
Object.freeze(logicalColumnValidationScope);
exports.default = logicalColumnValidationScope;
//# sourceMappingURL=logical.column.validation.scope.js.map