import { ServiceOrchestrator } from 'appeng-db';
import { Errors } from 'info-commons';
import { EntityGroupData, LogicalData } from '../models';
import { IValidationScope } from './';
export declare class LogicalEntityValidationScope implements IValidationScope {
    validateAll(entityGroupData: EntityGroupData, serviceOrchestrator: ServiceOrchestrator): Promise<Errors[]>;
    validateData(logicalData: LogicalData, serviceOrchestrator: ServiceOrchestrator, errorResponse: Errors[], referenceData: any, parentLocation: string): Promise<void>;
}
declare const logicalEntityValidationScope: LogicalEntityValidationScope;
export default logicalEntityValidationScope;
//# sourceMappingURL=logical.entity.validation.scope.d.ts.map