"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const cloneDeep_1 = __importDefault(require("lodash/cloneDeep"));
const constant_1 = require("../constant");
const executor_1 = require("../executor");
class LogicalEntityValidationScope {
    async validateAll(entityGroupData, serviceOrchestrator) {
        const errorResponse = [];
        await this.validateData(entityGroupData.logicalData, serviceOrchestrator, errorResponse, entityGroupData.referenceData, '');
        return errorResponse;
    }
    async validateData(logicalData, serviceOrchestrator, errorResponse, referenceData, parentLocation) {
        const logicalEntity = logicalData.ceNode.entity;
        let entityLocation = parentLocation +
            logicalEntity.configObjectId + constant_1.Separator.Navigate;
        for (const lcData of logicalData.appData.data) {
            const data = cloneDeep_1.default(lcData);
            Object.assign(data, referenceData);
            const errorList = await executor_1.logicalEntityDBValidator.validate(data, logicalData.ceNode.entity, data[constant_1.EXECUTION_MODE], serviceOrchestrator);
            if (errorList.length > 0) {
                const recordIndex = data[constant_1.RECORD_INDEX] !== undefined
                    ? data[constant_1.RECORD_INDEX] : logicalData.appData.data.indexOf(lcData);
                const entityResponse = new info_commons_1.Errors();
                entityResponse.message = errorList.toString();
                entityResponse.reason = errorList.toString();
                entityResponse.location = entityLocation + recordIndex;
                errorResponse.push(entityResponse);
            }
        }
        for (const ld of logicalData.childLogicalData) {
            entityLocation = entityLocation + '0' + constant_1.Separator.Navigate;
            await this.validateData(ld, serviceOrchestrator, errorResponse, referenceData, entityLocation);
        }
    }
}
exports.LogicalEntityValidationScope = LogicalEntityValidationScope;
const logicalEntityValidationScope = new LogicalEntityValidationScope();
Object.freeze(logicalEntityValidationScope);
exports.default = logicalEntityValidationScope;
//# sourceMappingURL=logical.entity.validation.scope.js.map