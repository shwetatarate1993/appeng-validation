import { ServiceOrchestrator } from 'appeng-db';
import { ErrorResponse } from 'info-commons';
import { DatabaseValidation } from '../models';
export declare class DBValidatorForClient {
    serviceOrchestrator: ServiceOrchestrator;
    constructor(serviceOrchestrator: ServiceOrchestrator);
    dbValidator(validation: DatabaseValidation, data: any): Promise<ErrorResponse>;
}
declare const createDBValidatorForClient: (serviceOrchestrator: ServiceOrchestrator) => DBValidatorForClient;
export default createDBValidatorForClient;
//# sourceMappingURL=dbvalidator.for.client.d.ts.map