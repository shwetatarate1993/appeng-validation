"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const constant_1 = require("../constant");
const appengutil_1 = require("../utils/appengutil");
class DBValidatorForClient {
    constructor(serviceOrchestrator) {
        this.serviceOrchestrator = serviceOrchestrator;
    }
    async dbValidator(validation, data) {
        const errorResponse = new info_commons_1.ErrorResponse();
        const result = await this.serviceOrchestrator.selectRecordForValidationObject(validation, data);
        if (!appengutil_1.isExpressionResolved(validation.validationExpression, result)) {
            const errors = [];
            const fieldResponse = new info_commons_1.Errors();
            fieldResponse.message = validation.validationMessage;
            fieldResponse.reason = validation.validationMessage;
            fieldResponse.location = validation.validationMessage;
            errors.push(fieldResponse);
            errorResponse.code = 406;
            errorResponse.message = constant_1.VALIDATION_FAILED;
            errorResponse.errors = errors;
        }
        else {
            errorResponse.code = 200;
            errorResponse.message = constant_1.VALIDATION_SUCCESS;
        }
        return errorResponse;
    }
}
exports.DBValidatorForClient = DBValidatorForClient;
let dbValidatorForClient;
const createDBValidatorForClient = (serviceOrchestrator) => {
    if (dbValidatorForClient === undefined || dbValidatorForClient === null) {
        dbValidatorForClient = new DBValidatorForClient(serviceOrchestrator);
        Object.freeze(dbValidatorForClient);
    }
    return dbValidatorForClient;
};
exports.default = createDBValidatorForClient;
//# sourceMappingURL=dbvalidator.for.client.js.map