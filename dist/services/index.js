"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var validate_service_1 = require("./validate.service");
exports.createValidateService = validate_service_1.default;
exports.Validator = validate_service_1.Validator;
var dbvalidator_for_client_1 = require("./dbvalidator.for.client");
exports.createDBValidatorForClient = dbvalidator_for_client_1.default;
exports.DBValidatorForClient = dbvalidator_for_client_1.DBValidatorForClient;
//# sourceMappingURL=index.js.map