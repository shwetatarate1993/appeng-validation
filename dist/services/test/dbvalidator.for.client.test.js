"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon_1 = __importDefault(require("sinon"));
const __1 = require("..");
const config_1 = __importDefault(require("../../config"));
const constant_1 = require("../../constant");
const init_config_1 = require("../../init-config");
const dbvalidator_mock_1 = __importDefault(require("../../mockdata/dbvalidator.mock"));
init_config_1.AppengValidationConfig.configure(config_1.default.get(constant_1.DB), 'sqlite3');
test('Validate entity group data', async () => {
    const data = JSON.parse(JSON.stringify(dbvalidator_mock_1.default));
    const servicesMock = init_config_1.AppengValidationConfig.DB_VALIDATOR_INSTANCE.serviceOrchestrator;
    const mocked = sinon_1.default.mock(servicesMock);
    mocked.expects('selectRecordForValidationObject').atLeast(1)
        .returns({ EXISTING_NAME_COUNT: 1 });
    const validator = new __1.DBValidatorForClient(servicesMock);
    const errorResponse = await validator.dbValidator(data.validation, data.data);
    sinon_1.default.assert.match(errorResponse.errors.length, 1);
});
//# sourceMappingURL=dbvalidator.for.client.test.js.map