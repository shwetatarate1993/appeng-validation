"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon_1 = __importDefault(require("sinon"));
const __1 = require("..");
const config_1 = __importDefault(require("../../config"));
const constant_1 = require("../../constant");
const init_config_1 = require("../../init-config");
const entity_group_data_1 = __importDefault(require("../../mockdata/entity.group.data"));
init_config_1.AppengValidationConfig.configure(config_1.default.get(constant_1.DB), 'sqlite3');
test('Validate entity group data', async () => {
    const data = JSON.parse(JSON.stringify(entity_group_data_1.default));
    const servicesMock = init_config_1.AppengValidationConfig.INSTANCE.serviceOrchestrator;
    const mocked = sinon_1.default.mock(servicesMock);
    mocked.expects('selectRecordForValidationObject').atLeast(1)
        .returns({ EXISTING_NAME_COUNT: 1 });
    const validator = new __1.Validator(servicesMock);
    const errorResponse = await validator.validateData(data);
    sinon_1.default.assert.match(errorResponse.errors.length, 2);
});
//# sourceMappingURL=validate.service.test.js.map