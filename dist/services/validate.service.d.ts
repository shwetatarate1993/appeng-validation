import { ServiceOrchestrator } from 'appeng-db';
import { ErrorResponse } from 'info-commons';
import { EntityGroupData } from '../models';
export declare class Validator {
    serviceOrchestrator: ServiceOrchestrator;
    constructor(serviceOrchestrator: ServiceOrchestrator);
    validateData(entityGroupData: EntityGroupData): Promise<ErrorResponse>;
}
declare const createValidateService: (serviceOrchestrator: ServiceOrchestrator) => Validator;
export default createValidateService;
//# sourceMappingURL=validate.service.d.ts.map