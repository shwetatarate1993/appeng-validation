"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const constant_1 = require("../constant");
const scope_1 = require("../scope");
class Validator {
    constructor(serviceOrchestrator) {
        this.serviceOrchestrator = serviceOrchestrator;
    }
    async validateData(entityGroupData) {
        console.log('service validateData..');
        const errors = [];
        const errorResponse = new info_commons_1.ErrorResponse();
        const columnsResponse = await scope_1.logicalColumnValidationScope
            .validateAll(entityGroupData, this.serviceOrchestrator);
        errors.push.apply(errors, columnsResponse);
        const entityResponse = await scope_1.logicalEntityValidationScope.validateAll(entityGroupData, this.serviceOrchestrator);
        errors.push.apply(errors, entityResponse);
        if (errors.length > 0) {
            errorResponse.message = constant_1.VALIDATION_FAILED;
            errorResponse.code = 406;
        }
        else {
            errorResponse.message = constant_1.VALIDATION_SUCCESS;
            errorResponse.code = 200;
        }
        errorResponse.errors = errors;
        // todo:need to implement for node
        return errorResponse;
    }
}
exports.Validator = Validator;
let validate;
const createValidateService = (serviceOrchestrator) => {
    if (validate === undefined || validate === null) {
        validate = new Validator(serviceOrchestrator);
        Object.freeze(validate);
    }
    return validate;
};
exports.default = createValidateService;
//# sourceMappingURL=validate.service.js.map