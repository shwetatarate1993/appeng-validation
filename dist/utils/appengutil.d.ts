export declare const isExpressionResolved: (expression: string, input: any) => any;
export declare const isExecutableWithMode: (columnMode: string, executionMode: string) => boolean;
//# sourceMappingURL=appengutil.d.ts.map