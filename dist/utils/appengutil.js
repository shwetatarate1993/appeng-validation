"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constant_1 = require("../constant");
exports.isExpressionResolved = (expression, input) => {
    expression = manipulateExpression(expression, input);
    try {
        /* tslint:disable-next-line:no-eval */
        return eval(expression);
    }
    catch (e) {
        return false;
    }
};
const manipulateExpression = (expression, input) => {
    const regexAnd = new RegExp('\\b' + 'and' + '\\b', 'ig');
    const regexOr = new RegExp('\\b' + 'or' + '\\b', 'ig');
    if (expression.startsWith('JAVASCRIPT$$')) {
        expression = expression.substring(expression.indexOf('$') + 2);
        expression = expression.replace(regexAnd, '&&').replace(regexOr, '||');
        return expression;
    }
    else {
        expression = expression.replace(/#/, '').replace(/{/, '').replace(/}/, '')
            .replace(regexAnd, '&&').replace(regexOr, '||');
        Object.entries(input).forEach(([key, value]) => {
            if (key !== '' && key !== undefined) {
                const reg = new RegExp('\\b' + key + '\\b', 'g');
                const valueConcat = 'input.' + key;
                expression = expression.replace(reg, valueConcat);
            }
        });
        return expression;
    }
};
exports.isExecutableWithMode = (columnMode, executionMode) => {
    return !columnMode || columnMode === constant_1.Mode.BOTH ||
        columnMode === executionMode;
};
//# sourceMappingURL=appengutil.js.map